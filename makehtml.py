import csv
import pystache

sheet = csv.reader(open('feeds.csv', encoding='utf-8'))
feeds = {}
index = 0
for url, text in sheet:
    index += 1
    print('{}: "{}", "{}"'.format(index, url, text))
    feeds['url{}'.format(index)] = url
    feeds['text{}'.format(index)] = text

open('index.html', 'w', encoding='utf-8').write(
    pystache.render(
         open('index.mustache', encoding='utf-8').read(),
         feeds))

print('Wrote {} items'.format(index))
