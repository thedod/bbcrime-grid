If you have Linux:

Run `./install.sh` once, and then you can

* `soffice feeds.csv`
* Save
* Run `./makehtml.sh`
* Enjoy your new `index.html`

If you have Windows: ask Eran.

Credits:
* [7 segment clock](http://www.3quarks.com/en/SegmentDisplay/) by [3Quarks](http://www.3quarks.com/index.html).
