echo Creating virtualenv...
virtualenv -p python3 venv
. venv/bin/activate
echo Installing dependencies...
pip install -r requirements.txt
echo Done.
echo Edit feeds.csv, then run ./doit.sh to generate index.html
